import { useState } from "react";

function Cadastrar({ addMember }) {
  const [name, setName] = useState("");
  const [type, setType] = useState("");

  const adicionar = () => {
    addMember(name, type);
    setName("");
    setType("");
  };

  return (
    <div className="container_cadastro">
      <div>
        <input
          type="text"
          value={name}
          placeholder="nome"
          className="name"
          onChange={(e) => setName(e.target.value)}
        ></input>
      </div>
      <div>
        <input
          type="text"
          value={type}
          placeholder="pf ou pj"
          className="type"
          onChange={(e) => setType(e.target.value)}
        ></input>
      </div>
      <div>
        <button className="botao_adicionar" onClick={adicionar}>
          Adicionar
        </button>
      </div>
    </div>
  );
}
export default Cadastrar;
