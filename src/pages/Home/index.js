import { Link } from "react-router-dom";

function Home({ members }) {
  return (
    <div>
      {members.map((membro) => (
        <Link
          key={membro.id}
          to={
            membro.type === "pj"
              ? `/company/${membro.id}`
              : `/customer/${membro.id}`
          }
        >
          <p>{membro.name}</p>
        </Link>
      ))}
    </div>
  );
}
export default Home;
