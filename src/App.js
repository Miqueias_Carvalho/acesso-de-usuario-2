import "./App.css";
import { Switch, Route } from "react-router-dom";
import { members } from "./members";
import Home from "./pages/Home";
import Customer from "./pages/customer";
import Company from "./pages/company";
import { useState } from "react";
import Cadastrar from "./components/Cadastrar";

function App() {
  //estado--------------------------------------------
  const [membros, setMembros] = useState(members);

  //função -----------------------------------------
  const addMember = (nome, tipo) => {
    const newId = Number(membros[membros.length - 1].id) + 1;
    const newMember = { id: newId.toString(), name: nome, type: tipo };

    setMembros([...membros, newMember]);
  };

  //jsx----------------------------------------------------
  return (
    <div className="App">
      <header className="App-header">
        <Switch>
          <Route exact path="/">
            <Home members={membros} />
            <Cadastrar addMember={addMember} />
          </Route>

          <Route exact path="/customer/:id">
            <Customer members={membros} />
          </Route>

          <Route exact path="/company/:id">
            <Company members={membros} />
          </Route>
        </Switch>
      </header>
    </div>
  );
}

export default App;
